//
//  ImageViewController.swift
//  ImageAssignment
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import UIKit
import SDWebImage

class ImageViewController: UIViewController {

    let viewModel = ImageDataViewModel(dataService: DataService())
    var imageListTableView: UITableView!
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTableView()
        self.getImageDataFromAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        imageListTableView.layoutIfNeeded()
    }
    
    func setUpTableView() {
        // TableView created and added
        imageListTableView = UITableView(frame: UIScreen.main.bounds)
        imageListTableView.register(ImageViewCell.self, forCellReuseIdentifier: "cell")
        imageListTableView.dataSource = self
        imageListTableView.delegate = self
        imageListTableView.translatesAutoresizingMaskIntoConstraints = false
        imageListTableView.separatorStyle = .none
        imageListTableView.estimatedRowHeight = 200
        imageListTableView.rowHeight = UITableView.automaticDimension
        self.view.addSubview(imageListTableView)
        imageListTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        imageListTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        imageListTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        imageListTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            imageListTableView.refreshControl = refreshControl
        } else {
            imageListTableView.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshImgeData(_:)), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Image Data ...")
    }
    
    @objc private func refreshImgeData(_ sender: Any) {
        getImageDataFromAPI()
        self.refreshControl.endRefreshing()
    }
    
    fileprivate func getImageDataFromAPI() {
        self.view.showSpinner()
        viewModel.fetchImageDataFromAPI()
        viewModel.didFinishFetch = {
            // After successfull data fetch, reload the table
            self.navigationItem.title = self.viewModel.navigationBarTitle
            DispatchQueue.main.async {
                self.view.hideSpinner()
                self.imageListTableView.reloadData()
            }
        }
        viewModel.didFailWithError = {
            self.view.hideSpinner()
            if let errorMessage = self.viewModel.errorMessage {
                // If case of error after API call, show alert stating the error occured
                self.refreshControl.endRefreshing()
                self.alert(message: errorMessage)
            }
        }
    }
}

extension UIViewController {
    // MARK: - Extension to show Alert on View Controller
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
